package com.matm.matmsdk.Dashboard;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.matm.matmsdk.Adapters.Settings.SettingRecyclerAdapter;
import com.matm.matmsdk.MPOS.MATM2ServiceBluetoothActivity;
import com.matm.matmsdk.Model.SettingList;
import isumatm.androidsdk.equitas.R;
import com.matm.matmsdk.Service.LoginSession;
import com.matm.matmsdk.Service.RecyclerTouchListener;
import com.paxsz.easylink.api.EasyLinkSdkManager;
import com.paxsz.easylink.model.KcvInfo;
import com.paxsz.easylink.model.KeyInfo;
import com.paxsz.easylink.model.KeyType;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static androidx.core.content.ContextCompat.checkSelfPermission;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {

    private List<SettingList> settingLists = new ArrayList<>();
    private RecyclerView recyclerView;
    private SettingRecyclerAdapter settingRecyclerAdapter;
    private EasyLinkSdkManager manager;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onDestroyView() {
        settingLists = null;
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View settingView =  inflater.inflate(R.layout.fragment_setting, container, false);

        manager = EasyLinkSdkManager.getInstance(getActivity());

        recyclerView = settingView.findViewById(R.id.settinglist);
        settingRecyclerAdapter = new SettingRecyclerAdapter(settingLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(settingRecyclerAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),LinearLayoutManager.VERTICAL));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                SettingList List = settingLists.get(position);
                if(List.getTitle() == "PAX Bluetooth Pair"){
                    Intent intent = new Intent(getContext(), MATM2ServiceBluetoothActivity.class);
                    if (checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                        Toast.makeText(getContext(),"Please Grant all the permissions", Toast.LENGTH_LONG).show();
                    } else {
                        startActivity(intent);
                    }

                }
                else if(List.getTitle() == "Logout"){
                    logout();
                }
                else if(List.getTitle() == "Download"){
                    //logout();
                    Intent intent = new Intent(getActivity(),FileDownLoadActivity.class);
                    startActivity(intent);
                }
                else if(List.getTitle() == "Key Inject"){
                    testWriteKey_TMK();
                    testWriteKey_EncryptTDK();
                    testWriteKey_EncryptTPK();
                }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareItem();

        return settingView;
    }

    private void prepareItem() {
        SettingList Ble = new SettingList(R.drawable.ic_bluetooth_logo,"PAX Bluetooth Pair");
        SettingList Logout = new SettingList(R.drawable.ic_home_black_24dp,"Logout");
        SettingList fileDownload = new SettingList(R.drawable.ic_download,"Download");
        SettingList keyInject = new SettingList(R.drawable.ic_download, "Key Inject");
        settingLists.add(Ble);
        settingLists.add(Logout);
        settingLists.add(fileDownload);
        settingLists.add(keyInject);
        settingRecyclerAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(settingRecyclerAdapter);
    }

    private void logout(){
        LoginSession.clearData(getContext());
        /*Intent loginIntent = new Intent(getContext(), LoginActivity.class);
        startActivity(loginIntent);*/

       // Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
       // loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //getActivity().startActivity(loginIntent);
    }

    // For TMK
    private void testWriteKey_TMK() {
        String s="0A227E33F5C0BEACD7B7DB09723ABB48";
        byte[] tmk = new BigInteger(s,16).toByteArray();
        KeyInfo keyInfo = new KeyInfo();
        keyInfo.setSrcKeyIndex((byte) 0);
        keyInfo.setSrcKeyType(KeyType.PED_NONE);
        keyInfo.setDstKeyIndex((byte) 30);
        keyInfo.setDstKeyLen((byte) tmk.length);
        keyInfo.setDstKeyType((byte) KeyType.PED_TMK);
        keyInfo.setDstKeyValue(tmk);

        KcvInfo kcvInfo = new KcvInfo();
        kcvInfo.setCheckMode((byte) 0);

        int ret = manager.writeKey(keyInfo, kcvInfo, 60);
        Log.d("MPOS","testWriteKey_TMK = " + ret);
    }
    // For TPK
    private void testWriteKey_EncryptTPK() {
        String s="48CA2E22B91BF61BE7DF4A66142AC2B6"; // actual key
        byte[] tpk = new BigInteger(s,16).toByteArray();
        KeyInfo keyInfo = new KeyInfo();
        keyInfo.setSrcKeyIndex((byte) 30);
        keyInfo.setSrcKeyType(KeyType.PED_TMK);
        keyInfo.setDstKeyIndex((byte) 32);
        keyInfo.setDstKeyLen((byte) tpk.length);
        keyInfo.setDstKeyType(KeyType.PED_TPK);
        keyInfo.setDstKeyValue(tpk);

        KcvInfo kcvInfo = new KcvInfo();
        kcvInfo.setCheckMode((byte) 0);
        int ret = manager.writeKey(keyInfo, kcvInfo, 60);
        Log.d("MPOS","testWriteKey_EncryptTPK = " + ret);

    }
// For TDK
    private void testWriteKey_EncryptTDK() {
        String s="53DFD3D6CE3D423FF9D4406601E0FA16"; // actual key
        byte[] tak = new BigInteger(s,16).toByteArray();
        KeyInfo keyInfo = new KeyInfo();
        keyInfo.setSrcKeyIndex((byte) 30);
        keyInfo.setSrcKeyType((byte) KeyType.PED_TMK);
        keyInfo.setDstKeyIndex((byte) 37);
        keyInfo.setDstKeyLen((byte) tak.length);
        keyInfo.setDstKeyType((byte) KeyType.PED_TDK);
        keyInfo.setDstKeyValue(tak);
        KcvInfo kcvInfo = new KcvInfo();
        kcvInfo.setCheckMode((byte) 0);
        int ret = manager.writeKey(keyInfo, kcvInfo, 60);
        Log.d("MPOS","testWriteKey_EncryptTDK = " + ret);
    }


    public String bcd2Str(byte[] bytes) {
        if (bytes == null) {
            return "";
        }
        char temp[] = new char[bytes.length * 2], val;

        for (int i = 0; i < bytes.length; i++) {
            val = (char) (((bytes[i] & 0xf0) >> 4) & 0x0f);
            temp[i * 2] = (char) (val > 9 ? val + 'A' - 10 : val + '0');

            val = (char) (bytes[i] & 0x0f);
            temp[i * 2 + 1] = (char) (val > 9 ? val + 'A' - 10 : val + '0');
        }
        return new String(temp);
    }
}
