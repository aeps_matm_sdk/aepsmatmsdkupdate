package com.matm.matmsdk.dmtModule.bankList;

import android.content.Context;

import java.util.ArrayList;

public class DMTBankContract {
    public interface View {
        void bankNameListReady(ArrayList<DMTBankModel> bankNameModelArrayList);

        void showBankNames();

        void showLoader();

        void hideLoader();

        void emptyBanks();
    }

    interface UserActionsListener {
        void loadBankNamesList(Context context);
    }

}
